package functions;

import org.junit.Test;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class LambdasTest {

    @Test
    public void helloWorldSupplier() {
        Supplier<String> supplier = Lambdas.helloWorldSupplier();

        assertThat(supplier.get(), is("Tere maailm"));
    }

    @Test
    public void isLengthy() {
        Predicate<String> lengthy = Lambdas.isLengthy();

        assertTrue(lengthy.test("Liiga pikk string selle testi jaoks"));
    }

    @Test
    public void notLengthy() {
        Predicate<String> lengthy = Lambdas.isLengthy();

        assertFalse(lengthy.test("short string"));
    }

    @Test
    public void stringLength() {
        Function<String, Integer> length = Lambdas.stringLength();

        assertThat(length.apply("Tere maailm"), is(11));
    }

    @Test
    public void concatenatedLength() {
        BiFunction<String, String, Integer> concatLength = Lambdas.concatenatedLength();

        assertThat(concatLength.apply("Tere", "Maailm"), is(10));
    }
}
