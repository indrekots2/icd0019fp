package functions;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Lambdas {

    /**
     * @return tagastab Supplier funktsiooni, mis tagastab alati "Tere maailm" stringi
     */
    public static Supplier<String> helloWorldSupplier() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return tagastab predikaadi, mis ütleb, kas String on liiga pikk või mitte.
     * String on liiga pikk kui see ületab 20 tähemärki.
     */
    public static Predicate<String> isLengthy() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return funktsioon, mis oskab öelda, kui pikk etteantud string on
     */
    public static Function<String, Integer> stringLength() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return funktsioon, mis võtab vastu kaks Stringi, liidab need kokku ja tagastab nende kogupikkuse.
     */
    public static BiFunction<String, String, Integer> concatenatedLength() {
        throw new UnsupportedOperationException("Implement me!");
    }
}
