package functions;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Function;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class FunctionMapTest {

    private FunctionMap<Integer, Integer> functionMap;

    @Before
    public void setUp() {
        functionMap = Functions.integerFunctionMap();
    }

    @Test
    public void abs() {
        Function<Integer, Integer> abs = functionMap.getFunctionByName("abs");

        assertThat(abs.apply(-22), is(22));
    }

    @Test
    public void square() {
        Function<Integer, Integer> abs = functionMap.getFunctionByName("square");

        assertThat(abs.apply(5), is(25));
    }

    @Test
    public void negate() {
        Function<Integer, Integer> negate = functionMap.getFunctionByName("negate");

        assertThat(negate.apply(-12), is(12));
        assertThat(negate.apply(34), is(-34));
    }

    @Test
    public void increment() {
        Function<Integer, Integer> increment = functionMap.getFunctionByName("increment");

        assertThat(increment.apply(4), is(5));
    }

    @Test
    public void decrement() {
        Function<Integer, Integer> increment = functionMap.getFunctionByName("decrement");

        assertThat(increment.apply(3), is(2));
    }

    @Test(expected = FunctionNotFoundException.class)
    public void unknownFunction() {
        functionMap.getFunctionByName("unknown");
    }
}
