package functions;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class FunctionMap<T, U> {

    private Map<String, Function<T, U>> functionMap;

    public FunctionMap() {
        functionMap = new HashMap<>();
    }

    public Function<T, U> getFunctionByName(String name) {
        if (functionMap.containsKey(name)) {
            return functionMap.get(name);
        } else {
            throw new FunctionNotFoundException(name);
        }
    }

    public void addFunction(String name, Function<T, U> function) {
        functionMap.put(name, function);
    }
}
