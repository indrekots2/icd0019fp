package absolutesum;

import java.util.List;

public class SumOfAbsoluteValues {

    public static int calculateSumOfAbsValues(List<Integer> values) {
        if (values == null) {
            throw new IllegalArgumentException("null is not allowed");
        }

        int sum = 0;
        for (Integer value : values) {
            sum = sum + Math.abs(value);
        }

        return sum;
    }
}
