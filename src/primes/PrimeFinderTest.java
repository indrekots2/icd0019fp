package primes;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static primes.PrimeFinder.findPrimesInRange;

public class PrimeFinderTest {

    @Test
    public void primesInEmptyRange() {
        List<Integer> primes = findPrimesInRange(2, 2);

        assertTrue(primes.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void startGreaterThanEnd() {
        findPrimesInRange(5, 1);
    }

    @Test
    public void primesIn0to10() {
        List<Integer> primes = findPrimesInRange(0, 10);

        assertThat(primes.size(), is(4));
        assertThat(primes, contains(2, 3, 5, 7));
    }

    @Test
    public void primesIn0To100() {
        List<Integer> primes = findPrimesInRange(0, 100);

        assertThat(primes.size(), is(25));
    }

    @Test
    public void rangeContainsNegativeNumbers() {
        List<Integer> primes = findPrimesInRange(-10, 15);

        assertThat(primes.size(), is(6));
        assertThat(primes, contains(2, 3, 5, 7, 11, 13));
    }



}
