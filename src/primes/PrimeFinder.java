package primes;

import java.util.ArrayList;
import java.util.List;

public class PrimeFinder {

    /**
     * Finds primes between start and end.
     * End is not included in the range.
     */
    public static List<Integer> findPrimesInRange(int start, int end) {
        if (start > end) {
            throw new IllegalArgumentException("Start cannot be greater than end");
        }

        // Algarv on naturaalarv, mis on suurem kui 1 ja mis jagub ainult arvuga 1 või iseendaga.

        List<Integer> primes = new ArrayList<>();
        for (int i = start; i < end; i++) {
            boolean isPrime = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime && i > 1) {
                primes.add(i);
            }
        }

        return primes;
    }
}
