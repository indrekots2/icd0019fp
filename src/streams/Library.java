package streams;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Library {

    private List<Book> books;

    public Library(List<Book> books) {
        this.books = books;
    }

    /**
     * @return list of authors whose books are in the library
     */
    public List<String> findAllAuthors() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return list of all book titles
     */
    public List<String> findAllBookTitles() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return book with the most pages in the library
     */
    public Optional<Book> findLongestBook() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return books grouped by author
     */
    public Map<String, List<Book>> booksByAuthor() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return first book with the given author
     */
    public Optional<Book> findFirstBookByAuthor(String author) {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * @return sum of all pages from all books in the Library
     */
    public int totalNumberOfPages() {
        throw new UnsupportedOperationException("Implement me!");
    }

    /**
     * Partitions books into two groups by the number of pages.
     */
    public Map<Boolean, List<Book>> partitionBooksByLength(int pages) {
        throw new UnsupportedOperationException("Implement me!");
    }

}
